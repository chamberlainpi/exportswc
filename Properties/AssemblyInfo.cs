using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("ExportSWC")]
[assembly: AssemblyDescription("Uses compc.exe from Flex sdk to compile SWC files from project resources")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Ali Chamas, Ben Babik-Charnock, David Rettenbacher")]
[assembly: AssemblyProduct("ExportSWC")]
[assembly: AssemblyCopyright("Ali Chamas, Ben Babik-Charnock, David Rettenbacher 2008 - 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyVersion("4.3.*")]

/* [Change Log]
 * 1.1.0.* - built against FD3b8, source released to SourceForge.net
 * 2.0.0.0 - FatSWC support
 * 2.1.*   - Settingsfile named like AS3Project file
 * 2.2.*   - Project file contextmenu integration
 * 2.3.*   - Workingdirectory and TreeView performance fix (18.2.2011)
 * 4.0.*   - FD4 Support (18.2.2011)
 * 4.1.*   - AIR Support (8.5.2011)
 * 4.2.*   - PluginCore.IPlugin change (2.6.2011)
 * 4.3.*   - Added append attribute to source-path and library path for compc config (2.5.2013)
 */