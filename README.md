# ExportSWC (fork of Alexx999 => darkwingduck => sourceforge.net) #

This is the source-files for the ExportSWC plugin used inside of FlashDevelop (for Exporting SWCs, as the name suggests).

### What is this repository for? ###

Since FlashDevelop itself and its 3rd-Party Plugins are open-source, I decided to contribute a bit of help fixing some bugs and adding features to this one specific plugin. I'm finding I use it quite a bit, and hate to see it fail/not do things properly.

### How do I get set up? ###

If you want to use the resulting ExportSWC.dll, there will be a download section available soon to obtain the latest copy.

On the other hand, if you wish to compile it yourself from the source, you can do so by cloning / forking this Git project and compiling the project from **Visual Studio Express** or your preferred C# IDE.
**Note:** Since it's hard for myself to keep track of absolute paths that may be set in the project configuration, you may have to dig into the project's properties to make any required changes to the dependencies' paths (*ex: PluginCore.dll, FlashDevelop.exe, ProjectManager.dll, ASCompletion.dll, AS3Context.dll*)

### Contribution guidelines ###

(Not currently accepting pull-requests)

### Who do I talk to? ###

For now you can send me a quick tweet at: @_bigp

Feel free to submit issues if you find some!